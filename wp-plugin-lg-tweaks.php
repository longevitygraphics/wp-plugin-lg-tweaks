<?php
/**
* Plugin Name: LG Tweaks
* Plugin URI: https://www.longevitygraphics.com/
* Author: Stefan Adam,
* Version: 1.0
*/ 
	remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );
?>
